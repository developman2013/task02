﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebPortal.Models;

namespace WebPortal.Controllers
{
    public class LinkController : Controller
    {
        private GroupsDBContext db = new GroupsDBContext();

        // GET: Link
        public ActionResult Index(int? id)
        {
            if (id == null)
                return View(db.Links.Where(link => link.UserName == User.Identity.Name).ToList());
            ViewBag.GroupId = id;
            ViewBag.GroupName = db.Groups.Find(id).Name;

            return View(db.Links.Where(p => p.GroupId == id).ToList());
        }

        // GET: Link/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LinkModel linkModel = db.Links.Find(id);
            if (linkModel == null)
            {
                return HttpNotFound();
            }
            if (linkModel.UserName != User.Identity.Name)
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Unauthorized"
                });

            return View(linkModel);
        }

        // GET: Link/Create
        public ActionResult Create(int groupId)
        {
            ViewBag.GroupId = groupId;
            return View();
        }

        // POST: Link/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,LongLink,GroupId,Status")] LinkModel linkModel)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Unauthorized"
                });

            if (ModelState.IsValid)
            {
                linkModel.RedirectedCount = 0;
                linkModel.ShortLink = "";
                linkModel.UserName = User.Identity.Name;
                linkModel.Status = true;

                db.Links.Add(linkModel);
                db.SaveChanges();

                string shortLink = $"{Request.Url.Scheme}://{Request.Url.Authority}{Request.ApplicationPath.TrimEnd('/')}/r/{linkModel.Id}";
                db.Links.Find(linkModel.Id).ShortLink = shortLink;
                db.SaveChanges();

                return RedirectToAction($"Index/{linkModel.GroupId}");
            }

            return View(linkModel);
        }

        // GET: Link/Edit/5
        public ActionResult Open(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LinkModel linkModel = db.Links.Find(id);
            if (linkModel.UserName != User.Identity.Name)
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Unauthorized"
                });

            if (linkModel == null)
            {
                return HttpNotFound();
            }
            var link = db.Links.Find(id);
            link.RedirectedCount += 1;
            db.SaveChanges();
            return Redirect(link.LongLink);
        }

        // POST: Link/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,LongLink,ShortLink,RedirectedCount,GroupId")] LinkModel linkModel)
        {
            if (ModelState.IsValid)
            {
                linkModel.UserName = User.Identity.Name;
                db.Entry(linkModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(linkModel);
        }

        // GET: Link/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LinkModel linkModel = db.Links.Find(id);
            if (linkModel.UserName != User.Identity.Name)
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Unauthorized"
                });

            if (linkModel == null)
            {
                return HttpNotFound();
            }
            db.Links.Remove(linkModel);
            db.SaveChanges();
            return Redirect(Request.UrlReferrer.ToString());
        }

        public ActionResult ChangeStatus(int id)
        {
            LinkModel linkModel = db.Links.Find(id);
            if (linkModel.UserName != User.Identity.Name)
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Unauthorized"
                });

            db.Links.Find(id).Status = !db.Links.Find(id).Status;
            db.SaveChanges();

            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
