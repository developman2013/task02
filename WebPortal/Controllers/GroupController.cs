﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebPortal.Models;

namespace WebPortal.Controllers
{
    public class GroupController : Controller
    {
        private GroupsDBContext db = new GroupsDBContext();

        // GET: Group
        public ActionResult Index()
        {
            if (!Request.IsAuthenticated)
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Unauthorized"
                });

            List<int> arrayLinkCounts = new List<int>(),
                      arrayRedirectLinkCounts = new List<int>();

            int redirectLinkCount = 0, linkCount = 0;
            var groups = db.Groups;
            foreach (var group in groups)
            {
                redirectLinkCount = linkCount = 0;

                db.Links
                    .Where(link => link.GroupId == group.Id)
                    .ForEachAsync(link => { linkCount++; redirectLinkCount += link.RedirectedCount; })
                    .GetAwaiter()
                    .GetResult();

                arrayLinkCounts.Add(linkCount);
                arrayRedirectLinkCounts.Add(redirectLinkCount);
            }
            ViewBag.ArrayLinkCounts = arrayLinkCounts;
            ViewBag.ArrayRedirectLinkCounts = arrayRedirectLinkCounts;

            return View(db.Groups.Include(p => p.Links).Where(p => p.UserName == User.Identity.Name));
        }

        // GET: Group/Details/5
        public ActionResult Details(int? id)
        {
            if (!Request.IsAuthenticated)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            GroupModel groupModel = db.Groups.Find(id);
            if (groupModel == null)
                return HttpNotFound();

            if (groupModel.UserName != User.Identity.Name)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            return View(groupModel);
        }

        // GET: Group/Create
        public ActionResult Create()
        {
            if (!User.Identity.IsAuthenticated)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            return View();
        }

        // POST: Group/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] GroupModel groupModel)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Unauthorized"
                });

            if (ModelState.IsValid)
            {
                groupModel.Status = true;
                groupModel.UserName = User.Identity.Name;
                db.Groups.Add(groupModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(groupModel);
        }

        // GET: Group/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Unauthorized"
                });

            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            GroupModel groupModel = db.Groups.Find(id);
            if (groupModel.UserName != User.Identity.Name)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            if (groupModel == null)
            {
                return HttpNotFound();
            }
            return View(groupModel);
        }

        // POST: Group/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] GroupModel groupModel)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Unauthorized"
                });

            if (groupModel.UserName != User.Identity.Name)
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Unauthorized"
                });

            if (ModelState.IsValid)
            {
                db.Entry(groupModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(groupModel);
        }

        // GET: Group/Delete/5
        public ActionResult Delete(int? id)
        {
            GroupModel groupModel = db.Groups.Find(id);
            if ((!User.Identity.IsAuthenticated) && (groupModel.UserName != User.Identity.Name))
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Unauthorized"
                });

            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (groupModel == null)
                return HttpNotFound();

            IQueryable<LinkModel> linksOnThisGroup = db.Links.Where(p => p.GroupId == id);
            db.Links.RemoveRange(linksOnThisGroup);
            db.Groups.Remove(groupModel);
            db.SaveChanges();
            return Redirect(Request.UrlReferrer.ToString());
        }

        public ActionResult ChangeStatus(int? id)
        {
            GroupModel groupModel = db.Groups.Find(id);
            if ((!User.Identity.IsAuthenticated) && (groupModel.UserName != User.Identity.Name))
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Unauthorized"
                });

            //Выключение группы
            bool newStatus = !db.Groups.Find(id).Status;
            db.Groups.Find(id).Status = newStatus;
            db.SaveChanges();

            //Выключение ссылок
            if (!newStatus)
            {
                var linksOnThisGroup = db.Links.Where(p => p.GroupId == id);
                foreach (LinkModel item in db.Links)
                {
                    item.Status = false;
                }
                db.SaveChanges();
            }

            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
