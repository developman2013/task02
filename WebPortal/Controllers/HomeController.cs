﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebPortal.Models;

namespace WebPortal.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        private GroupsDBContext db = new GroupsDBContext();

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Group");
            return RedirectToAction("About", "Home");
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Analysis()
        {
            string userName = User.Identity.Name;
            if (!Request.IsAuthenticated)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            //Database.SetInitializer(new DropCreateDatabaseAlways<GroupsDBContext>());
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<GroupsDBContext>());

            List<int> arrayLinkCounts = new List<int>(),
                      arrayRedirectLinkCounts = new List<int>();

            int redirectLinkCount = 0, linkCount = 0;
            var groups = db.Groups;
            foreach (var group in groups)
            {
                redirectLinkCount = linkCount = 0;

                db.Links
                    .Where(link => link.GroupId == group.Id)
                    .ForEachAsync(link => { linkCount++; redirectLinkCount += link.RedirectedCount; })
                    .GetAwaiter()
                    .GetResult();

                arrayLinkCounts.Add(linkCount);
                arrayRedirectLinkCounts.Add(redirectLinkCount);
            }
            ViewBag.ArrayLinkCounts = arrayLinkCounts;
            ViewBag.ArrayRedirectLinkCounts = arrayRedirectLinkCounts;

            return View(db.Groups.Include(p => p.Links).Where(p => p.UserName == User.Identity.Name));
        }
    }
}