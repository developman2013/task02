﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebPortal.Models
{
    public class LinkModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string LongLink { get; set; }
        public string ShortLink { get; set; }
        public int RedirectedCount { get; set; }
        public bool Status { get; set; }
        public int GroupId { get; set; }
    }

    public class GroupModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public bool Status { get; set; }
        public List<LinkModel> Links { get; set; } = new List<LinkModel>();
        //public int GroupRedirectedCount { get; set; }
        //public int LinkCount { get; set; }
    }

    public class GroupsDBContext : DbContext
    {
        public GroupsDBContext()
            :base("AzureConnectionString")
        { }
        public DbSet<GroupModel> Groups { get; set; }

        public DbSet<LinkModel> Links { get; set; }
    }
}