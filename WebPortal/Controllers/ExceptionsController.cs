﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPortal.Controllers
{
    public class ExceptionsController : Controller
    {
        // GET: Exceptions
        public ActionResult Unauthorized()
        {
            return View();
        }

        public ActionResult Ooops()
        {
            return View();
        }
    }
}