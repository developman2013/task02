﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortal.Models;

namespace WebPortal.Controllers
{
    public class RedirectorController : Controller
    {
        private GroupsDBContext db = new GroupsDBContext();

        public ActionResult Redirector(string linkId)
        {
            try
            {
                int id = Convert.ToInt32(linkId.Replace(" ", string.Empty));
                if (db.Links.Find(id).Status)
                {
                    db.Links.Find(id).RedirectedCount++;
                    db.SaveChanges();
                    return Redirect(db.Links.Find(id).LongLink);
                }
                else
                    return RedirectToRoute(new
                    {
                        controller = "Exceptions",
                        action = "Ooops"
                    });
            }
            catch
            {
                return RedirectToRoute(new
                {
                    controller = "Exceptions",
                    action = "Ooops"
                });
            }

            
        }

        public ActionResult Ooops(string titileCaption = null, string caption = null)
        {
            if ((titileCaption!= null) && (caption!= null))
            {
                ViewBag.TitleCaption = titileCaption;
                ViewBag.Caption = caption;
            }
            return View();
        }
    }
}