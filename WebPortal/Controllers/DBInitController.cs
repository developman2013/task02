﻿using System.Data.Entity;
using System.Web.Mvc;
using WebPortal.Models;

namespace WebPortal.Controllers
{
    public class DBInitController : Controller
    {
        private GroupsDBContext db = new GroupsDBContext();

        // GET: DBInit
        public ActionResult Initialize()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<GroupsDBContext>());
            //Database.SetInitializer(new DropCreateDatabaseAlways<GroupsDBContext>());

            return RedirectToRoute(new
            {
                controller = "Home",
                action = "About"
            });
        }
    }
}